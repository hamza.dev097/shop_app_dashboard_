import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app_dashboard/products.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Home();
  }
}

class _Home extends State<Home> {
  late List data;
  late Widget bodey = Center(child: Image.asset("assets/images/loding.gif",height: 200,width: 200,));
  @override
  void initState() {
    super.initState();
    getdata();
  }

  Future<String> getdata() async {
    var response = await http.get(Uri.parse(
        "http://10.224.132.255:1500/get_categories?email=info@kfc.com&password=kfc"));
    var jsondata = jsonDecode(response.body);
    setState(() {
      data = jsondata;
      bodey = GridView.builder(
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 191.6,
          childAspectRatio: 1 / 0.858,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5),
        itemCount: data.length,
        itemBuilder: (BuildContext ctx, index) {
          return InkWell(
            child:  Container(
              margin: EdgeInsets.all(5),
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Image.network("http://10.224.132.255:1500/get_categories_image?path=" + data[index]["logo"])
                  ),
                  SizedBox(height: 3.3),
                  Text(data[index]["name"],
                  style: TextStyle(color: Colors.black, fontSize: 18))
                ]
              )
            ),
            onLongPress: (){

              _onAlertButtonPressed(context,index);

            },
            onTap:(){
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => products(data[index]["id"])));


              } ,
          );
          });
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
          automaticallyImplyLeading: false,
        ),
        body: bodey,backgroundColor: Colors.white,);
  }
  _onAlertButtonPressed(context,index) {
    Alert(
      context: context,
      image: Image.network("http://10.224.132.255:1500/get_categories_image?path=" + data[index]["logo"]),
      title: ""+data[index]["name"]+"",
      desc: "Flutter is more awesome with RFlutter Alert.",
      buttons: [
        DialogButton(
          child: Text(
            "cancel",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
        DialogButton(
          child: Text(
            "upload",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {

          },
          width: 120,
        )


      ],
    ).show();
  }
}

