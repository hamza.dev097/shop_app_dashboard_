import 'dart:convert';

import 'Home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'bottomNavigationBar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  late String email;
  late String password;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Key key;

  bool visible = false;



  TextEditingController _controller = TextEditingController();
  TextEditingController _controller2 = TextEditingController();

  Future createAlbum(String email, String password) async {
    final response = await http.get(
      Uri.parse('http://10.224.132.255:1500/login_dashbord?email=' +
          email +
          '&password=' +
          password),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      if (response.body == "ok") {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    bottomNavigationBar(email, password)));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("err"),
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          SizedBox(
            height: 35,
            width: 35,
          ),
          Center(
            child: Text(
              'shop app dashboard',
              style: TextStyle(
                color: Colors.black,
                fontSize: 19,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 200),
          Container(
              child: Container(
                  width: 320,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    autocorrect: true,
                    controller: _controller,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.white70,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Color.fromRGBO(8, 168, 138, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Color.fromRGBO(8, 168, 138, 1),width: 2),
                      ),
                    ),
                  ))),
          Container(
              child: Container(
                  width: 320,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    autocorrect: true,
                    controller: _controller2,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Color.fromRGBO(8, 168, 138, 1)),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Color.fromRGBO(8, 168, 138, 1),width: 2),
                        )
                    ),
                  ))),
          SizedBox(height: 20),
          Container(
            width: 200,
            height: 60,
            child: Container(
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.00),
                  side: BorderSide(
                    color: Color.fromRGBO(8, 168, 138, 1),
                  ),
                ),
                color: Color(0xffffffff),
                padding: EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: MediaQuery.of(context).size.width / 3.5,
                ),
                child: Text(
                  'Login',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(8, 168, 138, 1)
                  ),
                ),
                onPressed: () {
                  createAlbum(_controller.text, _controller2.text);

                  //   Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => bottomNavigationBar()));
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
