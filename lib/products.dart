import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class products extends StatefulWidget {
  var id;
  products(var id_){
    id = id_;
  }
  @override
  State<StatefulWidget> createState() {
    return _products(id);
  }
}

class _products extends State<products> {
  late List data;
  var id;
  _products(var id_){
    id = id_;
  }

  Widget body = Center(child: Image.asset("assets/images/loding.gif",height: 200,width: 200,));
  @override
  void initState() {
    super.initState();

    getdata();

  }

  Future<String> getdata() async {
    var response = await http.get(Uri.parse("http://10.224.132.255:1500/get_products?email=info@kfc.com&product_types="+id.toString()+"&password=kfc"));
    setState(() {
      if(response.body.length<6) {
        body = Center(child: Image.asset(
          "assets/images/coming-soon.png", height: 200, width: 200,));
      }
    });
    var jsondata = jsonDecode(response.body);
    setState(() {
      data = jsondata;

      if(data.length>0) {
        body = GridView.builder(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 191.6,
                childAspectRatio: 1 / 0.858,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5),
            itemCount: data.length,
            itemBuilder: (BuildContext ctx, index) {
              return InkWell(
                child: Container(

                    margin: EdgeInsets.all(5),
                    child: Column(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.all(Radius.circular(
                                  20)),
                              child: Image.network(

                                  "http://10.224.132.255:1500/get_product_image?path=" +
                                      data[index]["logo"],



                              ),



                          ),
                          SizedBox(height: 3.3),
                          Text(data[index]["name"],
                              style: TextStyle(
                                  color: Colors.black, fontSize: 18))
                        ]
                    )
                ),
                onTap: () {},
              );
            });
      }
    });
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        automaticallyImplyLeading: false,
      ),
      body: body,backgroundColor: Colors.white,);
  }
}

