import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


class Chose_category extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Chose_category();
  }
}

class _Chose_category extends State<Chose_category> {
  late List data;
  late Widget bodey = Center(child: Image.asset("assets/images/loding.gif",height: 200,width: 200,));
  @override
  void initState() {
    super.initState();
    getdata();
  }
  Future<String> getdata() async {
    var response = await http.get(Uri.parse(
        "http://10.224.132.255:1500/get_categories?email=info@kfc.com&password=kfc"));
    var jsondata = jsonDecode(response.body);
    setState(() {
      data = jsondata;
      bodey = GridView.builder(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 191.6,
              childAspectRatio: 1 / 0.858,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5),
          itemCount: data.length,
          itemBuilder: (BuildContext ctx, index) {
            return InkWell(
              child:Container(
                margin: EdgeInsets.all(5),
                child: Column(children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Image.network("http://10.224.132.255:1500/get_categories_image?path=" + data[index]["logo"])),
                  SizedBox(height: 3.3),
                  Text(data[index]["name"],
                    style: TextStyle(color: Colors.black, fontSize: 18))
                ]
                )
              ),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => upload_products(data[index]["id"])));
              },
            );
          });
    });
    return "success";
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        automaticallyImplyLeading: false,
      ),
      body: bodey,backgroundColor: Colors.white,);
  }
}












class upload_products extends StatefulWidget {
  var product_id;
  upload_products(var productId){
    this.product_id = productId;
  }
  @override
  State<StatefulWidget> createState() {
    return _upload_products(product_id);
  }

}

class _upload_products extends State<upload_products> {
  late File _image = new File('your initial file');
  final picker = ImagePicker();
  var product_id;

  _upload_products(var productId) {
    this.product_id = productId;
  }

  upload_(File imageFile) async {
    var stream = new http.ByteStream(imageFile.openRead());
    var length = await imageFile.length();
    var uri = Uri.parse(
        "http://10.224.132.255:1500/add_product?email=info@kfc.com&password=kfc&name=" +
            _controllerName.text.toString() + "&product_type=" + product_id.toString() +
            "&price=" + _controllerPrice.text.toString() + "&product_description=" +
            _controllerDescription.text.toString() + "&status=1");
    var request = new http.MultipartRequest("POST", uri);
    var multipartFile = new http.MultipartFile(
        'file', stream, length, filename: (imageFile.path));
    request.files.add(multipartFile);
    var response = await request.send();
    print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
  }

  Future<void> getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerPrice = TextEditingController();
  TextEditingController _controllerDescription = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
        child: ListView(
          children: [
            TextField(
                autocorrect: true,
                controller: _controllerName,
                decoration: InputDecoration(
                  hintText: 'Name',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  fillColor: Colors.white,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(
                        color: Color.fromRGBO(8, 168, 138, 0.4), width: 2.3),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(
                        color: Color.fromRGBO(8, 168, 138, 1), width: 2.3),
                  ),
                )
            ),
            SizedBox(height: 20,),
            TextField(
                autocorrect: true,
                controller: _controllerDescription,
                decoration: InputDecoration(
                  hintText: 'Description',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  fillColor: Colors.white,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(
                        color: Color.fromRGBO(8, 168, 138, 0.4), width: 2.3),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(
                        color: Color.fromRGBO(8, 168, 138, 1), width: 2.3),
                  ),
                )
            ),
            SizedBox(height: 20,),
            TextField(
                autocorrect: true,
                controller: _controllerPrice,
                decoration: InputDecoration(
                  hintText: 'Price',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  fillColor: Colors.white,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(
                        color: Color.fromRGBO(8, 168, 138, 0.4), width: 2.3),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(
                        color: Color.fromRGBO(8, 168, 138, 1), width: 2.3),
                  ),
                )
            ),

            SizedBox(width: 20, height: 20,),
            InkWell(
              child: Container(
                width: 200,
                height: 50,
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                  color: Colors.blue,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),),
                ),
                child: Text("Select image"),

              ),
              onTap: () {
                getImage();
              },

            ),

            Image.file(

              _image,

              cacheHeight: 250,
              cacheWidth: 250,

            ),
            SizedBox(width: 20, height: 20,),
            SizedBox(width: 20, height: 20,),
            InkWell(
              child: Container(
                width: 200,
                height: 50,
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                  color: Colors.blue,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),),
                ),
                child: Text("upload"),

              ),
              onTap: () {
                _onAlertButtonPressed(context);

              },

            ),


          ],
        ),
      ),
    );
  }

  _onAlertButtonPressed(context) {
    Alert(
      context: context,
      image: Image.file(_image),
      title: "gg",
      desc: "Flutter is more awesome with RFlutter Alert.",
      buttons: [
        DialogButton(
          child: Text(
            "cancel",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
        DialogButton(
          child: Text(
            "upload",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => upload_(_image),
          width: 120,

        )


      ],
    ).show();
  }

}





