import 'package:flutter/material.dart';
import 'package:shop_app_dashboard/upload_category.dart';
import 'package:shop_app_dashboard/upload_products.dart';

import 'bottomNavigationBar.dart';

class upload extends StatefulWidget {
  const upload({Key? key}) : super(key: key);

  @override
  _upload createState() => _upload();
}

class _upload extends State<upload> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: GridView(
                padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 46.0),
                scrollDirection: Axis.vertical,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 20.0,
                  childAspectRatio: 0.7),
                children: <Widget>[
                  InkWell(

                  child:  Container(
                    color: Colors.black12,
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 87, 0, 0),
                    child: Column(children: [
                      Icon(
                        Icons.add,
                        size: 70,
                      ),
                      Text("Add category", style: TextStyle(fontSize: 20))
                    ])),
                    onTap: (){Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => upload_category()));},
                  ),

                  InkWell(

                    child:  Container(
                      color: Colors.black12,
                      alignment: Alignment.center,
                      padding: EdgeInsets.fromLTRB(0, 87, 0, 0),
                      child: Column(
                        children: [
                          Icon(
                            Icons.add,
                            size: 70,
                          ),
                          Text("Upload prodect", style: TextStyle(fontSize: 20))
                        ]
                      )
                    ),
                    onTap: (){Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Chose_category()));},
                  )
        ])));
  }
}
