import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
class upload_category extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _upload_categoryState();
  }

}

class _upload_categoryState extends State<upload_category> {
  late String email;
  late String password;
  TextEditingController _controller = TextEditingController();
  late File _image = new File('your initial file');

  final picker = ImagePicker();

  Future<void> getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
  upload(File imageFile) async {
    var stream =
    new http.ByteStream(imageFile.openRead());

    var length = await imageFile.length();

    var uri = Uri.parse("http://10.224.132.255:1500/add_category?email=info@kfc.com&password=kfc&name="+_controller.text);

    var request = new http.MultipartRequest("POST", uri);
    var multipartFile = new http.MultipartFile('file', stream, length,
        filename:(imageFile.path));
    request.files.add(multipartFile);
    var response = await request.send();
    print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
        child: Column(
          children: [
            TextField(
              autocorrect: true,
              controller: _controller,
                decoration: InputDecoration(
                  hintText: 'Type Text Here...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  fillColor: Colors.white,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(color: Color.fromRGBO( 8, 168, 138,0.4),width: 2.3),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(color: Color.fromRGBO( 8, 168, 138,1),width: 2.3),
                  ),
                )
            ),
            SizedBox(width: 20,height: 20,),
            InkWell(
              child: Container(
                width: 200,
                height: 50,
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                    color: Colors.blue,
                    borderRadius: new BorderRadius.only(
                        topLeft:  const  Radius.circular(20.0),
                        topRight: const  Radius.circular(20.0),
                        bottomLeft: const  Radius.circular(20.0),
                        bottomRight: const  Radius.circular(20.0),),
                ),
                child: Text("Select image"),

              ),
              onTap: (){
                getImage();
              },

            ),
            Image.file(

                _image,
              cacheHeight: 300,
              cacheWidth: 300,

            ),
            SizedBox(width: 20,height: 20,),
            InkWell(
              child: Container(
                width: 200,
                height: 50,
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                  color: Colors.blue,
                  borderRadius: new BorderRadius.only(
                    topLeft:  const  Radius.circular(20.0),
                    topRight: const  Radius.circular(20.0),
                    bottomLeft: const  Radius.circular(20.0),
                    bottomRight: const  Radius.circular(20.0),),
                ),
                child: Text("uplod"),

              ),
              onTap: (){
             //  upload(_image);
                _onAlertButtonPressed(context);
              },

            ),

          ],
        ),
      ),
    );
  }
  _onAlertButtonPressed(context) {
    Alert(
      context: context,
      image: Image.file(_image),
      title: "gg",
      desc: "Flutter is more awesome with RFlutter Alert.",
      buttons: [
        DialogButton(
          child: Text(
            "cancel",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
        DialogButton(
          child: Text(
            "upload",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => upload(_image),
          width: 120,
        )


      ],
    ).show();
  }
}
